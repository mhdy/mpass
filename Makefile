PREFIX := ~/.local

install:
	mkdir -p $(PREFIX)/bin/
	install -m 0700 mpass $(PREFIX)/bin/

uninstall:
	rm -f $(PREFIX)/bin/mpass

clean:
	@echo "nothing to do"

.PHONY: clean uninstall install
