# mpass - a POSIX shell simple password manager

mpass is a POSIX compliant password manager intended to be simple at use.

Compared to [pass](https://www.passwordstore.org/), it features a tag system that lets you annotate the passwords instead of organizing them in groups.
This way, one password can be annotated with many tags and thus can belong to many groups.
Plus, it is written in POSIX shell instead of bash, and it will be kept simple: you can create, get, update, remove and search passwords, no new features will be added other than code optimization.

## Installation

Clone this repo. The best release is what is in the master branch.

```
git clone https://gitlab.com/mhdy/mpass.git
```

Then, type `make install` which will copy the mpass script to `~/.local/bin/mpass`.
If you want to install it system-wide, copy the mpass script to `/usr/bin`, then give it the right permissions `chmod 755 /usr/bin/mpass`.

## Usage

### Password store initialization

First, if not done yet, create a GPG key pair using `gpg --full-gen-key`, then initialize a password store with your GPG key identifier, as follows:

```sh
mpass init GPG_KEY
```

The default path to the password store is `~/.password-store/`. However, you can choose another path by setting the environment variable `MPASS_STORE_PATH`, as follows:

```sh
export MPASS_STORE_PATH=/full/path/to/store
mpass init GPG_KEY
# or
MPASS_STORE_PATH=/full/path/to/store mpass init GPG_KEY
```

### Creation of a new password entry

Each password entry is identified by a unique identifier, is annotated with tags, and has 3 main attributes: a URL, a username and a password.

To create a new password entry, type:

```sh
mpass create -l URL -u USERNAME ID TAG1 TAG2...
```

This will prompt for a password.

If you want to pass a password as an argument, you can use the `-p PASSWORD` option.
However, this is not recommended because it can stay in your command history.

Note that during the creation, the identifier and the password are required, but the other fields (URL, username and tags) are not.

You can generate a password as follows:

```sh
mpass create -l URL -u USERNAME -g13 ID TAG1 TAG2...
```

Here, the length of the generated password is 13. If no argument is provided to the `-g` option, a default value of 17 is used for the length.

### Getting a password

To show the password only, type:

```sh
mpass get ID
```

Or simpler:

```sh
mpass ID
```

To show the password and all its related data, type:

```sh
mpass get -a ID
# or
mpass show ID
```

To get some information, for example, the URL and the username:

```sh
mpass get -l -u ID
```

To copy the password, type:

```sh
mpass copy ID    # copies to the primary selection (middle click to paste)
mpass copy -c ID # copies to the clipboard (control-v to paste)
```

### Updating a password entry

To update a password entry, the accepted options are:

```
-i, --identifier NEW_ID   Specify a new identifier.
-t, --tags TAGS           Specify comma separated tags.
-l, --url URL             Specify the URL.
-u, --username USERNAME   Update the username.
-p, --password PASSWORD   Update the password.
-v, --verbose             Show a report of the updated password entry.
    --help                Display this help and exit.
```

These are some examples:

```sh
mpass update -u NEW_USERNAME ID                     # change the username
mpass update -u NEW_USERNAME -l NEW_URL ID          # change the username and the URL
mpass update -p NEW_PASSWORD -t tag1,tag2,tag3 ID   # change the password and the associated tags
mpass update -i NEW_ID ID                           # change the ID
```

### Editing a password entry

One can supplement additional information to the password entry, for instance, secret questions or a PIN code. To do so, type:

```sh
mpass edit ID
```

This will bring your editor specified by the `EDITOR` environment variable, or nano by default.
The information is concatenated with the password entry and the temporary file used to record the data is shredded right after closing the editor.

### Searching for a password

You can display the list of passwords annotated by some tag as follows:

```sh
mpass search TAG
# or
mpass search TAG1 TAG2...
```

If you provide many tags, the search will be performed for each tag apart.

### Deleting a password entry

Simply type:

```sh
mpass delete ID
```

### Getting help

```sh
mpass --help
# or
mpass CMD --help
```

where CMD is one of: create, update, etc.

## License

MIT.
